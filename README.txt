 
 HOW IT WORKS:
 
 Using an autocomplete vocabulary which is set for "tagging" and "hiearchy", 
 any time a machine tag is added (toplevel_term:subterm:subsubterm), it is parsed
 into a group of individual terms, making taxonomy based views and other searches
  more rich.
 
 Activate via the admin control pannel for a vocabulary by selecting "machine tags".
 
