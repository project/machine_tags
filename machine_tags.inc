<?php
/*
 * 
 * Copyright (C) 2008  Jonathan Hendler 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * The authors can be contacted at: 
 *  - [hendlerman (at) yahoo DOT com]
 * 
 * @license GPLv2
 * @link    http://drupal.org/project/machine_tags
 */
 
 /**
  * the prefix for system variables
  * */
  define('MACHINE_TAGS_PREFIX' , 'machine_tags_');
  
 /**
  * alters the admin form for a given Vocabulary to enable machine tags
  * */
 function _machine_tags_form_alter_admin( &$form) {
     
    $new_form = array();
    $vid = $form['vid']['#value'];
    //rebuilds the form array, inserting machine tags checkbox after 'tags' checkbox
    foreach ($form as $key => $value) {
         $new_form[$key]=$value;
         if ($key == 'tags') {
           $new_form['machine_tags'] = array(
              '#type' => 'checkbox',
              '#title' => 'Machine tagging',
              '#default_value' =>  variable_get(MACHINE_TAGS_PREFIX . $vid, false),   
              '#description' => t('Allows the creation of machine tags. <strong>Requires free tagging, and single or multiple hiearchy.</strong>')
            );
         }
    }
    
    $form = $new_form;
 }
 
 /**
  * convert machine tags to regular drupal taxonomy
  * 
  * notes: 
  *   - insert or update shouldn't matter, since inserting or updating is checked here.
  *   - delete is ignored since it is ambiguous to delete all the terms of 
  *     fruit:apple when fruit:pear might still be valid
  * */
 function _machine_tags_process($tid, $name, $op, $vid) {
    //check if the tag is a machine tag
    //don't bother to process tag if it is a delete op
    if ( _machine_tags_validate_tag($name) && $op != 'delete') {
        //tokenize the string
        $tokens = _machine_tags_tokenize($name);
        $parents = array();
        $parent = null;
        foreach ($tokens as $token) {
          //lookup token to see if this will be an insert or update
          $tid = _machine_tags_get_tid($token, $vid, $parent);
          
          if ($tid <1) { //insert  
            $tid = _machine_tags_insert($vid, $token, $parents);
          }
          else { //update  
            _machine_tags_update($vid, $token, $parents, $tid);
          }
          
          //set parents ( note that all of these have the given 
          //parent id provided as a way to track the relationships )
          //and add the tid to the parents of the next term
          $parents[$token] = $parent = $tid;
        }
    }
 }
 
 /**
  * check if a string is a valid machine tag
  * @param string 
  * @return boolean
  * */
  function _machine_tags_validate_tag($string) {
     //should be more sophisticated regex, just testing for now
     return strstr(trim($string), ':');
  }
  
  /**
  * check if a string is a valid machine tag
  * @param string 
  * @return array
  * */
  function _machine_tags_tokenize(&$string) {
     //should be more sophisticated regex, just testing for now
     return explode(':', strtolower(trim($string)));
  }
  
  /**
  * check if a string is a valid machine tag
  * @param string 
  * @return array
  * */
  function _machine_tags_tokenize_multiple(&$string) {
     //should be more sophisticated regex, just testing for now
     return explode(',', strtolower($string));
  }
 
  /**
  * check if term object matches this term
  * 
  * @param string 
  * @return mixed, null or a matching $term object
  * */
  function _machine_tags_get_tid($token, $vid, $parent) {
      if ($parent !== null) {
        $sql = "SELECT t.tid FROM {term_data} t, {term_hierarchy} th WHERE t.vid=%d AND LOWER('%s') LIKE LOWER(t.name) AND t.tid=th.tid AND th.parent=%d ";
        $db_result = db_query_range( db_rewrite_sql($sql, 't', 'tid'), $vid, strtolower(trim($token)), $parent, 0, 1);
      } 
      else {
        $sql = "SELECT t.tid FROM {term_data} t WHERE t.vid=%d AND LOWER('%s') LIKE LOWER(t.name) ";
        $db_result = db_query_range( db_rewrite_sql($sql, 't', 'tid'), $vid, strtolower(trim($token)), 0, 1);
      }
      
      $result = db_fetch_array($db_result);
      return (empty($result)) ? 0 : $result['tid'];
  }
  
  /**
   * machine tag insert
   * @return int a tid
   * */
  function _machine_tags_insert( $vid, $token, &$parents) {
     $tid = db_next_id('term_data_tid');
     $sql = "INSERT INTO {term_data} (name, vid) VALUES('%s',%d) ";
     db_query($sql, strtolower(trim($token)), $vid);
     if (!empty($parents)) {
       _machine_tags_edit_parents_to_tid($parents, $tid);
     }
     return $tid;
  }
  
  /**
   * machine tag update
   * */
  function _machine_tags_update( $vid, $token, &$parents, $tid) {
     $sql = "UPDATE {term_data} SET name='%s' WHERE tid=%d AND vid=%d";
     db_query($sql, strtolower(trim($token)), $tid, $vid);
     if (!empty($parents)) {
       _machine_tags_edit_parents_to_tid($parents, $tid);
     }
  }
  
  /**
   * machine tag helper function for inserting terms
   * note that the parents are ALL parents, instead of depending
   * on the taxonomy model to do inference
   * 
   * @param array parents[term] = tid;
   * @param int term id
   * */
  function _machine_tags_edit_parents_to_tid(&$parents, $tid) {
     foreach ($parents as $parent) {
        //check for duplicates
        $sql = "SELECT t.tid FROM {term_hierarchy} t WHERE t.tid=%d AND t.parent=%d ";
        $db_result = db_query_range( db_rewrite_sql($sql, 't', 'tid'), $tid, $parent, 0, 1);
        $result = db_fetch_array($db_result);
        if (empty($result)) {
          $sql = "INSERT INTO {term_hierarchy} (tid, parent) VALUES(%d,%d) ";
          db_query($sql, $tid, $parent);
        }
     }
  }
  
  /**
   * machine tag helper function for hook_nodeapi()
   * @param array tids[] = tid;
   * @param int node id
   * */
  function _machine_tags_edit_tids_to_node(&$tids, $nid) {
     foreach ($tids as $tid) {
      //check for duplicates
      $sql = "SELECT t.tid FROM {term_node} t WHERE t.tid=%d AND t.nid=%d ";
      $db_result = db_query_range( db_rewrite_sql($sql, 't', 'tid'), $tid, $nid, 0, 1);
      $result = db_fetch_array($db_result);
       if (empty($result)) {
          $sql = "INSERT INTO {term_node} (tid, nid) VALUES(%d,%d) ";
          db_query($sql, $tid, $nid);
       }
     }
  }
  